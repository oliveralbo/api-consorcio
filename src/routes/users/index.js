const express = require("express");
const router = express.Router();
// const Users = require("../models/hanged.js");
const Users = require("../../models/users.js");

router.get("/", async (req, res) => {
  const users = await Users.find();
  res.send(users);
});

router.post("/add", async (req, res) => {
  console.log(req && req.body)
  const users = new Users(req.body);
  try {
    await users.save();
    res.send("se grabo correctamente");
    // res.redirect("/");
  } catch {
    res.send("fallo la carga d datos. vuelva a intentarlo mas tarde!!");
  }
  // console.log(req);
});

router.get("/delete/:id", async (req, res) => {
  const { id } = req.params;
  await Users.remove({ _id: id });
  res.send("se borro correctamente");
  // res.redirect("/");
});

router.get("/turn/:id", async (req, res) => {
  const { id } = req.params;
  const users = await Users.findById(id);
  users.status = !users.status;
  await users.save();
  res.redirect("/");
});

router.get("/edit/:id", async (req, res) => {
  const { id } = req.params;
  const users = await Users.findById(id);

  res.render("edit", { users });
});

router.post("/edit/:id", async (req, res) => {
  const { id } = req.params;
  await users.update({ _id: id }, req.body);

  res.redirect("/");
});

module.exports = router;
