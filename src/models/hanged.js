const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const hangedSchema = new Schema({
  word: String,
  status: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model("hanged", hangedSchema);
