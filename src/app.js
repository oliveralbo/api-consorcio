const path = require("path");
const express = require("express");
const app = express();
const morgan = require("morgan");
const mongoose = require("mongoose");
const cors = require('cors');
bodyParser = require('body-parser');
// import cors from "cors";

//connecting to db
mongoose
  .connect("mongodb://localhost/api-consorcio")
  .then(db => console.log("db connected"))
  .catch(err => console.log(err));

//importing routes
//ej: import indexRoutes from './routes'
const hangedRoutes = require("./routes/hanged");
const usersRoutes = require("./routes/users");

//settings
app.use(cors());
app.set("port", process.env.PORT || 5000);
// app.set("views", path.join(__dirname, "views"));
// app.set("view engine", "ejs");

//middlewares
app.use(morgan("dev"));
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json()); 



//routes
app.use("/users", usersRoutes);

// starting the server
app.listen(app.get("port"), () => {
  console.log(`escuchando puerto http://localhost:${app.get("port")}`);
});
